Source: bordeaux-threads
Section: lisp
Priority: optional
Maintainer: Debian Common Lisp Team <debian-common-lisp@lists.debian.org>
Uploaders: Sébastien Villemot <sebastien@debian.org>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.6.0
Homepage: https://common-lisp.net/project/bordeaux-threads/
Vcs-Git: https://salsa.debian.org/common-lisp-team/bordeaux-threads.git
Vcs-Browser: https://salsa.debian.org/common-lisp-team/bordeaux-threads
Rules-Requires-Root: no

Package: cl-bordeaux-threads
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         cl-alexandria
Recommends: cl-fiveam
Description: Portable threads library for Common Lisp
 BORDEAUX-THREADS is a proposed standard for a minimal MP/Threading interface.
 .
 It essentially provides a compatibility layer for multi-threading across
 multiple CL implementations.
 .
 Some parts of its implementation-specific code can also be implemented in a
 Lisp that does not support multiple threads, so that thread-safe code can be
 compiled on both multithread and single-thread implementations without need of
 conditionals.
