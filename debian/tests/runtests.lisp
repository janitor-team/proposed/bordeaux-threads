(require "asdf")

(let ((asdf:*user-cache* (uiop:getenv "AUTOPKGTEST_TMP"))) ; Store FASL in some temporary dir
  (asdf:load-system "bordeaux-threads/test"))

;; Can't use ASDF:TEST-SYSTEM, its return value is meaningless
(let ((results (5am:run :bordeaux-threads)))
  (5am:explain! results)
  (unless (5am:results-status results)
    (uiop:quit 1)))
